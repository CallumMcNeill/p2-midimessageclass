//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include <cmath>

class MidiMessage
{
public:
    MidiMessage()//Constuctor
    {
        noteNumber = 60;
        velocity = 127;
        channel = 1;
        std::cout << "Constructor\n";
    }
    MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel)
    {
        noteNumber = initialNoteNumber;
        velocity = initialVelocity;
        channel = initialChannel;
        std::cout << "Constructor\n";
    }
    ~MidiMessage()//Destructor
    {
        std::cout << "Destructor\n";
    }
    void setNoteNumber (int value) //Mutator
    {
        if (value >= 0)
        {
            noteNumber = value;
        }
        else
        {
            std::cout << "Number Invalid\n";
        }
    }
    int getNoteNumber() const //Accessor
    {
        return noteNumber;
    }
    float getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (noteNumber-69) / 12.0);
    }
    void setVelocity (int value) //Mutator
    {
        if (value >= 0)
        {
            velocity = value;
        }
        else
        {
            std::cout << "Number Invalid\n";
        }
    }
    int getVelocity() const //Accessor
    {
        return velocity;
    }
    void setChannel (int value) //Mutator
    {
        if (value >= 0)
        {
            channel = value;
        }
        else
        {
            std::cout << "Number Invalid\n";
        }
    }
    int getChannel() const //Accessor
    {
        return channel;
    }
    float getFloatVelocity() const //Accessor
    {
        return (velocity/1.0583333333) - 96;
    }

private:
    int noteNumber;
    int velocity;
    int channel;
};
int main()
{
   // int readNum;
   // MidiMessage note;
   //
   // std::cout << "Default value of note: " << note.getNoteNumber() << "\n";
   // std::cout << "Default value of frequency: " << note.getMidiNoteInHertz() << "\n";
   // std::cout << "Insert new note number: \n";
   //
   // std::cin >> readNum;
   // note.setNoteNumber(readNum);
   //
   // std::cout << "New value of inserted note: " << note.getNoteNumber() << "\n";
   // std::cout << "New value of inserted frequency: " << note.getMidiNoteInHertz() << "\n";
   //
   // std::cout << "\nInsert note Velocity: \n";
   //
   // std::cin >> readNum;
   // note.setVelocity(readNum);
   //
   // std::cout << "Velocity: " << note.getVelocity() << "\n";
   // std::cout << "Amplitude: " << note.getFloatVelocity() << "\n";
   //
    
    //Testing function overloading
    MidiMessage note (50, 50, 50);
    
    std::cout << "Note Number: " << note.getNoteNumber() << "\n";
    std::cout << "Velocity: " << note.getVelocity() << "\n";
    std::cout << "Channel: " << note.getChannel() << "\n";
    
    return 0;
}
